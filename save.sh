#!/usr/bin/env bash
echo -e bandit$(( $(wc -l passwords.txt | awk '{print $1}') + 1))"\t$1" >> passwords.txt
